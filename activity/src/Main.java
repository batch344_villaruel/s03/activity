import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed");
        int num = 0;
        try {
           num = in.nextInt();
            if (num <= 0) {
                System.out.println("Factorial is not defined for zero or negative numbers.");
            } else {
                int answer = 1;
                int counter = 1;
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }
                System.out.println("Factorial of " + num + " is " + answer);

                int answerForLoop = 1;
                for (int i = 1; i <= num; i++) {
                    answerForLoop *= i;
                }
                System.out.println("Factorial of " + num + " using a for loop is " + answerForLoop);
            }
            } catch (Exception e) {
            System.out.println("Invalid input, numbers only");
            e.printStackTrace();
        }
    }
}